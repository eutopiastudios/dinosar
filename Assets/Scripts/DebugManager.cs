﻿using UnityEngine;

public class DebugManager : MonoBehaviour
{
    private UIController uiController;

    // Below needed to test within editor
#if UNITY_EDITOR

    void Start()
    {
        uiController = GameObject.Find("GameCanvas").GetComponent<UIController>();
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            GameManager.GM.DebugText("Clicked");

            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log(hit.transform.name);
                if (hit.transform.tag == "Button")
                {
                    uiController.CallMove(hit.transform.name);
                }
                else if (hit.transform.tag == "Ground")
                {
                    Debug.Log("Move");
                    GameManager.GM.PlayerMovement.MoveTo(hit.point);
                }
                else
                {
                    return;
                }
            }
        }
    }
#endif
}
