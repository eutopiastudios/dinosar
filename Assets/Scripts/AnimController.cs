﻿using UnityEngine;

public class AnimController : MonoBehaviour
{
    private Animator DinoAnim;
    public bool isIdle = false;

    void OnEnable()
    {
        DinoAnim = GetComponent<Animator>();
    }

    public void PlayAnim(string Anim)
    {
        DinoAnim.Play(Anim);
        if (GameManager.GM.DebugOn)
        {
            GameManager.GM.DebugText(Anim);
        }
    }
}
