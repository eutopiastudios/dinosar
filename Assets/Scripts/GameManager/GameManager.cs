﻿using UnityEngine;
using UnityEngine.XR;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager GM;
    public bool DebugOn = false;
    public Transform ARCam;

    public string XRType;
    public string XRModel;

    public string Modeltext;
    public Transform MessageBox;

    public PlayerController PlayerDino;
    public PlayerMovement PlayerMovement;

    void Awake()
    {
        if (GM == null)
        {
            GM = this;
            DontDestroyOnLoad(this.gameObject);
        }

        else if (GM != this)
        {
            Destroy(gameObject);
        }

        if (Debug.isDebugBuild)
        {
            DebugOn = true;
        }
    }

    void Start()
    {
        XRType = XRSettings.loadedDeviceName;
        XRModel = XRDevice.model;
        Modeltext = getSystemInfo();

        //ARCam = GameObject.Find("ARCamera").transform;

        PlayerDino = GameObject.FindGameObjectWithTag("Player").AddComponent<PlayerController>();
        PlayerMovement = GameObject.FindGameObjectWithTag("Player").AddComponent<PlayerMovement>();

        if (DebugOn)
        {
            DebugText("Debug Mode On" + getSystemInfo());
            gameObject.AddComponent<DebugManager>();
        }
    }

    public string getSystemInfo()
    {
        string str = "<color=red>SYSTEM INFO</color>";

#if UNITY_EDITOR_WIN
        str += "\n[system info]" + SystemInfo.graphicsDeviceID;
#endif

#if UNITY_ANDROID
        str += "\n[system info]" + SystemInfo.deviceModel;
#endif

        str += "\n[type]" + SystemInfo.deviceType;
        str += "\n[os version]" + SystemInfo.operatingSystem;
        str += "\n[system memory size]" + SystemInfo.systemMemorySize;
        str += "\n[graphic device name]" + SystemInfo.graphicsDeviceName + " (version " + SystemInfo.graphicsDeviceVersion + ")";
        str += "\n[graphic memory size]" + SystemInfo.graphicsMemorySize;
        str += "\n[graphic max texSize]" + SystemInfo.maxTextureSize;
        str += "\n[graphic shader level]" + SystemInfo.graphicsShaderLevel;
        str += "\n[support compute shader]" + SystemInfo.supportsComputeShaders;

        str += "\n[processor count]" + SystemInfo.processorCount;
        str += "\n[processor type]" + SystemInfo.processorType;
        str += "\n[support 3d texture]" + SystemInfo.supports3DTextures;
        str += "\n[support shadow]" + SystemInfo.supportsShadows;

        str += "\n[platform] " + Application.platform;
        str += "\n[screen size] " + Screen.width + " x " + Screen.height;
        str += "\n[screen pixel density dpi] " + Screen.dpi;

        return str;
    }

    public void DebugText(string showText)
    {
        Transform debugCanvas = GameObject.Find("DebugCanvas").transform;
        debugCanvas.GetComponentInChildren<Text>().text = showText;
    }
}
