﻿using UnityEngine;

public class HitDetector : MonoBehaviour {

    private string Attack;
    public float damage;
    public Transform Character;

    void OnTriggerEnter(Collider other)
    {
        if(other.transform.name != this.name)
        {
            GameManager.GM.DebugText(this.name + " hit " + other.transform.name);
        }
    }
}
