﻿using UnityEngine;

public class UIController : MonoBehaviour
{

    void Update()
    {
        if (Input.touchCount <= 0)
            return;

        // Button Selection
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            Ray ray = Camera.main.ScreenPointToRay(new Vector3(touch.position.x, touch.position.y, 0));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.transform.tag == "Button")
                {
                    CallMove(hit.transform.name);
                }
                else if(hit.transform.tag == "ground")
                {
                    GameManager.GM.PlayerMovement.MoveTo(touch.position);
                }
                else
                {
                    return;
                }
            }
        }

        if (Input.touchCount == 2)
        {
            Touch touch = Input.GetTouch(1);

            if (GameManager.GM.DebugOn)
            {
                GameManager.GM.DebugText("Two fingers detected");
            }
        }

        if (Input.touchCount == 3)
        {
            Touch touch = Input.GetTouch(2);

            if (GameManager.GM.DebugOn)
            {
                GameManager.GM.DebugText("Three fingers detected");
            }
        }
    }

    public void CallMove(string ButtonText)
    {

    }
}
