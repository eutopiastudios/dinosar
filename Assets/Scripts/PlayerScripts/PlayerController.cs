﻿using UnityEngine;

// Script used to control player character animations
public class PlayerController : MonoBehaviour {

    private Transform player;
    public Animator DinoAnim;

    void Start()
    {
        player = transform;
        DinoAnim = player.GetComponent<Animator>();
        DinoAnim.enabled = true;
    }

}
