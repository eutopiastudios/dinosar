﻿using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour {

    private NavMeshAgent agent;
    private PlayerController pcontroller;

    void OnEnable()
    {
        agent = transform.GetComponent<NavMeshAgent>();
        pcontroller = transform.GetComponent<PlayerController>();
    }

    public void MoveTo(Vector3 pos)
    {
        Debug.Log("Moving");
        agent.destination = pos;
        pcontroller.DinoAnim.SetBool("Move", true);
    }
}
